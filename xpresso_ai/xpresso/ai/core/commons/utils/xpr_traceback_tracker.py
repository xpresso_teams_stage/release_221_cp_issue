""" Object for traceback storage """

__all__ = ["XprTracebackTracker"]
__author__ = "Mrunalini Dhapodkar"

from flask import request
from xpresso.ai.core.commons.utils.singleton import Singleton


class XprTracebackTracker(metaclass=Singleton):
    """

    Defines an Instance operation that lets events collection
    access traceback for every API call wrt request ID.

    """

    def __init__(self):
        super().__init__()
        self.traceback_map = {}

    def store_traceback(self, traceback):
        """

        Maps error traceback to current request ID and stores this mapping
        Args:
            traceback: exception traceback for request ID for API call

        """

        request_id = request.environ.get("HTTP_X_REQUEST_ID")
        self.traceback_map[request_id] = traceback

    def return_traceback(self):
        """
        Returns: traceback wrt current request ID for logging
        """
        request_id = request.environ.get("HTTP_X_REQUEST_ID")
        if request_id not in self.traceback_map.keys():
            return None
        return self.traceback_map[request_id]
